package br.com.guest.jpa2019.teste;

public class Magazine {
	private Long id;
	private String isbn;
	private String title;
	private Double price;
	private Integer copiesSold;
	private Integer version;
	
	public Magazine() {
		super();
	};
	public Magazine(Long id, String isbn, String title, Double price, Integer copiesSold, Integer version) {
		super();
		this.isbn = isbn;
		this.title = title;
		this.price = price;
		this.copiesSold = copiesSold;
		this.version = version;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Integer getCopiesSold() {
		return copiesSold;
	}
	public void setCopiesSold(Integer copiesSold) {
		this.copiesSold = copiesSold;
	}
	public Integer getVersion() {
		return version;
	}
	public void setVersion(Integer version) {
		this.version = version;
	};
};
