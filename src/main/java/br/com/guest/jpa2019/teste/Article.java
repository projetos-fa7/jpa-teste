package br.com.guest.jpa2019.teste;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Article {
	@Id	
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String title;
	private byte[] content;
	private int version;
	
	//@OneToOne
	//private Article coverArticle;
	//@ManyToOne
	//private Article articles;
	
	public Long getId() {
		return id;
	}
	
	public Article() {}
	
	public Article(Long id, String title, byte[] content, int version) {
		super();
		this.id = id;
		this.title = title;
		this.content = content;
		this.version = version;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public byte[] getContent() {
		return content;
	}
	public void setContent(byte[] content) {
		this.content = content;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	
}
