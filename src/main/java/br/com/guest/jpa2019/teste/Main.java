package br.com.guest.jpa2019.teste;

import java.util.Collection;

public class Main {

	public static void main(String[] args) {
		
		UsuarioDAO dao = new UsuarioDAO();
		
		Usuario usuario = new Usuario(null, "Roberio", "123456");
		
		dao.inserir(usuario);
		
		
		dao.remover(2);
		
		Collection<Usuario> usuarios = dao.buscarTodos();
		usuarios.forEach(u -> {
			System.out.println(u);
		});
		
		System.out.println(dao.buscarPorId(1));
	}

}
