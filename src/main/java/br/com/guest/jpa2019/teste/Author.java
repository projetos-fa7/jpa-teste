package br.com.guest.jpa2019.teste;

public class Author {
	private Long id;
	private String firstName;
	private String lastName;
	private Integer version;
	public Author() {}
	public Author(Long id, String firstName, String lastName, Integer version) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.version = version;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Integer getVersion() {
		return version;
	}
	public void setVersion(Integer version) {
		this.version = version;
	};
	
}