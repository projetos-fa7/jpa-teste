package br.com.guest.jpa2019.teste;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class UsuarioDAO {
	
	private EntityManager em;
	
	public UsuarioDAO() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("uni7-pu");
		this.em = emf.createEntityManager();
	}
	
	public Usuario inserir(Usuario usuario) {
		em.getTransaction().begin();
		em.persist(usuario);
		em.getTransaction().commit();
		return null;
	}
	
	public List<Usuario> buscarTodos() {
		em.getTransaction().begin();
		List<Usuario> rl = em.createQuery("FROM Usuario").getResultList();
		em.getTransaction().commit();
		return rl;
	}
	
	public Usuario buscarPorId(Integer id) {
		em.getTransaction().begin();
		Query q = em.createQuery("FROM Usuario WHERE id = :id");
		q.setParameter("id", id);
		Usuario u = (Usuario) q.getSingleResult();
		em.getTransaction().commit();
		return u;
	}
	
	public void remover(Integer id) {
		em.getTransaction().begin();
		Query q = em.createQuery("DELETE Usuario WHERE id = :id");
		q.setParameter("id", id);
		q.executeUpdate();
		em.getTransaction().commit();
	}

}
