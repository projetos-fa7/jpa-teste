package br.com.guest.jpa2019.teste;

public enum StatusType {
	ATIVO,
	PENDENTE,
	BLOQUEADO
}
