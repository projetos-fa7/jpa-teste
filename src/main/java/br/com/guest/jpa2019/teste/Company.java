package br.com.guest.jpa2019.teste;

public class Company {
	private Long id;
	private String name;
	private Double revenue;
	private Integer version;
	public Company() {}
	public Company(Long id, String name, Double revenue, Integer version) {
		super();
		this.id = id;
		this.name = name;
		this.revenue = revenue;
		this.version = version;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getRevenue() {
		return revenue;
	}
	public void setRevenue(Double revenue) {
		this.revenue = revenue;
	}
	public Integer getVersion() {
		return version;
	}
	public void setVersion(Integer version) {
		this.version = version;
	}
	
};