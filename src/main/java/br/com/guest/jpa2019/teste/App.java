package br.com.guest.jpa2019.teste;

import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class App 
{
    public static void main( String[] args )
    {
    	Properties config = new Properties();
		config.put("hibernate.hbm2ddl.auto", "create");
		
    	EntityManagerFactory factory = Persistence.
				createEntityManagerFactory("uni7-pu", config);
		EntityManager em = factory.createEntityManager();
		em.close();
		factory.close();
		
    }
}
