package br.com.guest.jpa2019.teste;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Vaga {
	
	@Id
	private Integer id;
	private String logradouro;
	
	public Vaga(){}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public Vaga(Integer id, String logradouro) {
		super();
		this.id = id;
		this.logradouro = logradouro;
	}
	
	
}
