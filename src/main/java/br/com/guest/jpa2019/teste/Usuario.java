package br.com.guest.jpa2019.teste;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TB_USR")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Usuario {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "CD_USR")
	private Integer id;
	
	@Column(name = "LOGIN")
	private String username;
	
	@Column(name = "SENHA")
	private String password;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DT_ADM")
	private Calendar dataAdmissao;
	
	@Column(name = "STATUS")
	@Enumerated(EnumType.ORDINAL)
	private StatusType status;
	
	
	@OneToMany(mappedBy = "usuario", cascade = { CascadeType.PERSIST, CascadeType.REMOVE }, fetch = FetchType.EAGER)
	private List<Veiculo> veiculos;
	
	public Usuario() {
		this(null, null, null);
	}
	
	public Usuario(Integer id, String username, String password) {                         
		this.id = id;
		this.username = username;
		this.password = password;
		this.dataAdmissao = new GregorianCalendar();
		this.status = StatusType.ATIVO;
		this.veiculos = new ArrayList<Veiculo>();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public Calendar getDataAdmissao() {
		return dataAdmissao;
	}
	
	public void setDataAdmissao(Calendar dataAdmissao) {
		this.dataAdmissao = dataAdmissao;
	}
	
	public StatusType getStatus() {
		return status;
	}
	
	public void setStatus(StatusType status) {
		this.status = status;
	}

	public List<Veiculo> getVeiculos() {
		return veiculos;
	}

	public void setVeiculos(List<Veiculo> veiculos) {
		this.veiculos = veiculos;
	}
	
	public void addVeiculo(Veiculo v) {
		this.veiculos.add(v);
		v.setUsuario(this);
	}
	
	public String toString() {
		return this.id + " : " + this.username;
	}
	
}
